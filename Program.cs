﻿using System;

namespace ex_14
{
    class Program
    {
        static void Main(string[] args)
        {
            // --------------------------------------------
            // Exercise #14 - Get a value from a user
            // --------------------------------------------
            
            // Set a variable needed.
            var userBirthMonth = "";
            var userAnswerOfBirthMonth ="";
            Boolean isFished = false;

            // Get user's birthday
            Console.WriteLine("-----------------------------------");
            Console.WriteLine(" Let me ask you some information.");
            Console.WriteLine("-----------------------------------");

            while(!isFished)
            {
                Console.Write("What month are you born in (ex:March or 03)?  : ");
                userBirthMonth = Console.ReadLine();
                Console.WriteLine($"\nYour birth month is {userBirthMonth}\n");

                // --------------------------------------------
                // Exercise #17 - Creating a simple app with a single variable
                // --------------------------------------------
                Console.WriteLine($"Is your birth month({userBirthMonth}) correct (Yes/No)?");
                userAnswerOfBirthMonth = Console.ReadLine().ToLower();
                if(userAnswerOfBirthMonth == "yes" || userAnswerOfBirthMonth == "y")
                {
                    Console.WriteLine("Thank you!\n\n");
                    isFished = true;
                }
                else
                {
                    Console.WriteLine("The answer is wrong. Please type in your birth month again");
                }
            }
        }
    }
}
